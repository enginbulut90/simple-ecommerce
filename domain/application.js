module.exports = function (id = 0, name = '', secret = '', enableTwoFactorAuth = false) {
	this.Id = id;
	this.Name = name;
	this.TwoFactorEnabled = enableTwoFactorAuth; 
	this.Secret = secret;
};

