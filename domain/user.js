module.exports = function (username = '', password = '', id = 0, applications = []) {
	this.Id = id;
	this.Username = username;
	this.Password = password;
	this.Applications = applications;
};