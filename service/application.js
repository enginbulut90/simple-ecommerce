const { fetch, save } = require('../models/appmodel');
const Application = require('../domain/application');

const jwt = require('jsonwebtoken');
const { imobaSecret } = require('../config');

const generateSecret = name => jwt.sign(name, imobaSecret);

const saveApp = async (appname, enableTwoFactor) => {
	const app = new Application(0, appname, generateSecret(appname), enableTwoFactor);
	const existingApp = await fetch({ Name: app.Name });
	app.Secret = existingApp ? existingApp.Secret : app.Secret;
	return await save(app);
};

module.exports = {
	fetch: fetch,
	save: saveApp,
	generateSecret: generateSecret
};