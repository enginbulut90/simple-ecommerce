const jwt = require('jsonwebtoken');

const { fetch: fetchUser } = require('../models/usermodel');
const { fetch: fetchApp } = require('../models/appmodel');
const { imobaSecret } = require('../config');
const md5Crypto = require('crypto-js/md5');

const generateToken = user => jwt.sign(user, imobaSecret);
const getRandomInt = max => Math.floor(Math.random() * Math.floor(max));
const sixDigitRandom = () => getRandomInt(999999);

const generateTwoFactorToken = (userid) => {
	const smsvalue = sixDigitRandom();
	return { token : jwt.sign({ userid: userid, smsvalue: smsvalue }, imobaSecret, { expiresIn: '1h' }), smsvalue: smsvalue } ;
};

const authenticate = async (username, password, appsecret) => {
	const md5pwd = md5Crypto(password, imobaSecret).toString();
	const application = await fetchApp({ Secret: appsecret });
	if (!application) {
		return undefined;
	}
	const user = await fetchUser({ Username: username, Password: md5pwd });
	if (user && application.TwoFactorEnabled) {
		return generateTwoFactorToken(user.Id);
	}
	return user ? { token : generateToken({ ...user }) } : undefined;
};

const verifyToken = async (token, appsecret) => {
	const application = await fetchApp({ Secret: appsecret });
	if (!application) {
		return undefined;
	}
	return jwt.verify(token, imobaSecret);
};

const validateTwoFactor = async (token, smsvalue, appsecret) => {
	const application = await fetchApp({ Secret: appsecret });
	if (!application) {
		return undefined;
	}
	const decryptedToken = await verifyToken(token, appsecret);
	if (decryptedToken.smsvalue === smsvalue) {
		const user = await fetchUser({ _id: decryptedToken.userid });
		return user ? generateToken({ ...user }) : undefined;
	}
	return undefined;
};

module.exports = {
	authenticate: authenticate,
	verifyToken: verifyToken,
	validateTwoFactor: validateTwoFactor
};