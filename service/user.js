const User = require('../domain/user');
const { imobaSecret } = require('../config');
const md5Crypto = require('crypto-js/md5');
const jwt = require('jsonwebtoken');

const { save: saveUser, fetch: fetchUser, remove: removeUser, generateUser } = require('../models/usermodel');
const { fetch: fetchApp } = require('../models/appmodel');

const getAppBySecret = async secret => await fetchApp({ Secret: secret });
const getUserByUsername = async username => await fetchUser({ Username: username });

const createUser = async (applicationUser, appsecret) => {
	const username = applicationUser.username;
	const password = applicationUser.password;
	if (!username || !password) {
		return undefined;
	}
	const app = await getAppBySecret(appsecret);
	if (!app) {
		return undefined;
	}
	const md5pwd = md5Crypto(password, imobaSecret);
	const user = await getUserByUsername(username);
	if (user) {
		const existingAppId = user.Applications.find(t => t == app.Id);
		if (!existingAppId) {
			return await saveUser(generateUser(applicationUser, new User(username, md5pwd, user.Id, [...user.Applications, app.Id])));
		}
		// Do nothing since user is already the apps user. 
		return await saveUser(generateUser(applicationUser, new User(username, md5pwd, user.Id, [...user.Applications])));
	}
	return await saveUser(generateUser(applicationUser, new User(username, md5pwd, 0, [app.Id])));
};

const getUsers = async (condition, appsecret) => {
	const app = await getAppBySecret(appsecret);
	if (!app) {
		return undefined;
	}
	const users = await fetchUser(Object.assign({ ...condition }, { Applications: { $elemMatch: { $eq: app.Id } } }));
	return !users ? [] : users;
};

const resetPasswordRequest = async (userid, appsecret) => {
	const user = await getUsers({ _id: userid }, appsecret);
	if (!user) {
		return undefined;
	}
	return jwt.sign({ ...user }, imobaSecret, { expiresIn: '1h' });
};

const resetPassword = async (token, appsecret, newPassword) => {
	const user = await getUsers({ _id: jwt.verify(token, imobaSecret).Id }, appsecret);
	if (!user) {
		return undefined;
	}
	user.Password = md5Crypto(newPassword, imobaSecret);
	return await saveUser(user);
};

const deleteUser = async (userId, appsecret) => {
	const app = await getAppBySecret(appsecret);
	if (!app) {
		return false;
	}
	const user = await fetchUser({ _id: userId });
	if (!user) {
		return false;
	}
	const userApps = [...user.Applications];
	if (userApps) {
		const updatedApps = user.Applications.filter(t => t != app.Id);
		if (updatedApps.length === 0) {
			return await removeUser({ _id: userId });
		} else {
			await saveUser(new User(user.Username, user.Password, user.Id, updatedApps));
			return true;
		}
	}
	return await removeUser({ _id: userId });
};

module.exports = {
	createUser: createUser,
	getUsers: getUsers,
	removeUser: deleteUser,
	resetPasswordRequest: resetPasswordRequest,
	resetPassword: resetPassword
};
