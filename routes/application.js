const router = new (require('restify-router')).Router();
const logger = require('../basic-logger');

const appService = require('../service/application');

router.get('/', async function (req, res, next) {
	try {
		const apps = await appService.fetch();
		res.send(apps);
		next();
	} catch (ex) {
		logger.error(ex);
		res.send(500);
		next();
	}
});

router.post('/', async function (req, res, next) {
	try {
		const body = req.body;
		if (!body || !body.appName) {
			res.send(400, 'Check out API documentation');
			return next();
		}
		res.send(await appService.save(body.appName, body.enableTwoFactor));
		next();
	} catch (ex) {
		logger.error(ex);
		res.send(500);
		next();
	}
});

module.exports = router;
