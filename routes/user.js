const router = new (require('restify-router')).Router();
const logger = require('../basic-logger');
const userService = require('../service/user');
const { JsonWebTokenError } = require('jsonwebtoken');

router.post('/', async function (req, res, next) {
	try {
		const secret = req.header('App-Secret');
		const createUserBody = req.body;
		if (!secret || !createUserBody || !createUserBody.username || !createUserBody.password) {
			res.send(400, { message: 'Check out API documentation' });
			return next();
		}
		const createdUser = await userService.createUser(createUserBody, secret);
		createdUser ? res.send(createdUser) : res.send(400, { message: 'Check out API documentation' });
		next();
	} catch (ex) {
		logger.error(ex);
		res.send(500);
		next();
	}
});

const getUser = condition => async (req, res, next) => {
	try {
		const secret = req.header('App-Secret');
		if (!secret) {
			res.send(400, { message: 'Check out API documentation' });
			return next();
		}
		const users = await userService.getUsers(condition, secret);
		users === undefined ?
			res.send(400, { message: 'Application not found' }) :
			res.send(users);
		next();
	} catch (ex) {
		logger.error(ex);
		res.send(500);
		next();
	}
};

router.get('/', getUser({}));
router.get('/:id', (req, res, next) => getUser({ _id: req.params.id })(req, res, next));

const removeUser = async function (req, res, next) {
	try {
		const secret = req.header('App-Secret');
		if (!secret) {
			res.send(400, { message: 'Check out API documentation' });
			return next();
		}
		await userService.removeUser(req.params.userid, secret) ? res.send(200) : res.send(400);
		next();
	} catch (ex) {
		logger.error(ex);
		res.send(500);
		next();
	}
};

router.del('/:userid', removeUser);
router.get('/delete/:userid', removeUser);

router.get('/resetpassword/:userid', async function (req, res, next) {
	try {
		const secret = req.header('App-Secret');
		if (!secret) {
			res.send(400, { message: 'Check out API documentation' });
			return next();
		}
		const token = await userService.resetPasswordRequest(req.params.userid, secret);
		token ? res.send({ token: token }) : res.send(400, { message: 'Check out API documentation' });
	} catch (ex) {
		logger.error(ex);
		res.send(500);
		next();
	}
});

router.post('/resetpassword/:resettoken', async function (req, res, next) {
	try {
		const secret = req.header('App-Secret');
		if (!secret || !req.body || !req.body.newpassword) {
			res.send(400, { message: 'Check out API documentation' });
			return next();
		}
		const success = await userService.resetPassword(req.params.resettoken, secret, req.body.newpassword);
		success ? res.send(200) : res.send(400, { message: 'Check out API documentation' });
	} catch (ex) {
		logger.error(ex);
		typeof ex === typeof JsonWebTokenError ? res.send(400, { message: ex.message }) : res.send(500);
	}
	next();
});

module.exports = router;
