const router = new (require('restify-router')).Router();
const logger = require('../basic-logger');

const accountService = require('../service/account');

router.post('/authenticate', async function (req, res, next) {
	const secret = req.header('App-Secret');
	const authbody = req.body;

	if (!secret || !authbody || !authbody.username || !authbody.password) {
		res.send(400, { message: 'Check out API documentation' });
	}
	try {
		const token = await accountService.authenticate(authbody.username, authbody.password, secret);
		token ? res.send(token) : res.send(401);
	} catch (ex) {
		logger.error(`Exception : ${ex} Body : ${JSON.stringify(req.body)} App Secret : ${secret}`);
		res.send(500);
	}
	next();
});

router.get('/validatetoken', async function (req, res, next) {
	const secret = req.header('App-Secret');
	const authtoken = req.header('iMobaAuthToken');
	if (!secret || !authtoken) {
		res.send(400, { message: 'Check out API documentation' });
	}
	try {
		const success = await accountService.verifyToken(authtoken, secret);
		success ? res.send(200) : res.send(401);
	} catch (ex) {
		logger.error(ex);
		if (ex.name === 'SyntaxError') {
			res.send(401, { message: 'Invalid token' });
		}
		typeof ex === typeof JsonWebTokenError ? res.send(401, { message: ex.message }) : res.send(500);
	}
	next();
});

router.post('/validatetwofactor', async function (req, res, next) {
	const secret = req.header('App-Secret');
	const twofactortoken = req.body.token;
	const smsinput = req.body.smsinput;
	if (!secret || !twofactortoken || !smsinput) {
		res.send(400, { message: 'Check out API documentation' });
		next();
	}
	try {
		const token = await accountService.validateTwoFactor(twofactortoken, smsinput, secret);
		token ? res.send({ token: token }) : res.send(401);
	} catch (ex) {
		logger.error(ex);
		if (ex.name === 'SyntaxError') {
			res.send(401, { message: 'Invalid token' });
			return next();
		}
		typeof ex === typeof JsonWebTokenError ? res.send(401, { message: ex.message }) : res.send(500);
	}
});

module.exports = router;