require('../dataaccess/src/mongo');
const Application = require('../domain/application');
const mongoose = require('mongoose');
const util = require('util');
const { save, convertToModels, fetch, remove } = require('../dataaccess/src/mongohelper');
const Schema = mongoose.Schema;

const applicationSchema = new Schema({
	Name: String,
	Secret: String,
	TwoFactorEnabled: Boolean,
}, { versionKey: false });

const ApplicationModel = mongoose.model('Applications', applicationSchema, 'Applications');

const convertToModel = (app = new Application()) => {
	const model = new ApplicationModel();
	model.Name = app.Name;
	model.Secret = app.Secret;
	model.TwoFactorEnabled = app.TwoFactorEnabled;
	// Mongo auto generated id is not used and fails the updates.
	delete model._doc._id;
	return model;
};

const appSelector = app => {
	return {
		Name: app.Name
	};
};

const convertToDomain = (applicationModel = new ApplicationModel()) => {
	return util.isArray(applicationModel)
		?
		applicationModel.map(u => new Application(u.id, u.Name, u.Secret, u.TwoFactorEnabled))
		:
		new Application(applicationModel.id, applicationModel.Name, applicationModel.Secret, applicationModel.TwoFactorEnabled);
};

const fetchApp = async (condition = new Object()) => {
	const appdoc = await fetch(ApplicationModel)(condition);
	return appdoc ? convertToDomain(appdoc) : null;
};

const saveApp = async entities => {
	const saveEntities = save(ApplicationModel, convertToModels(convertToModel), appSelector);
	const savedEntity = await saveEntities(entities);
	return savedEntity ? convertToDomain(savedEntity) : undefined;
};

const removeApp = async (condition = new Object()) => await remove(ApplicationModel)(condition);

module.exports = {
	ApplicationModel: ApplicationModel,
	convertToModel: convertToModels(convertToModel),
	save: saveApp,
	convertToDomain: convertToDomain,
	fetch: fetchApp,
	remove: removeApp
};