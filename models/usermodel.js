const util = require('util');
require('../dataaccess/src/mongo');
const User = require('../domain/user');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { save, convertToModels, fetch, remove } = require('../dataaccess/src/mongohelper');

const userSchema = new Schema({
	Username: String,
	Password: String,
	Applications: [String]
}, { strict: false, versionKey: false });

const UserModel = mongoose.model('Users', userSchema, 'Users');

const convertToModel = user => {
	const userClone = { ...user };
	delete userClone.Id;
	const model = new UserModel(userClone);
	delete model._doc._id;
	return model;
};

const generateUser = (rawUser, domainUser = new User()) => {
	const applicationUserClone = Object.assign({}, rawUser);
	delete applicationUserClone.username;
	delete applicationUserClone.password;
	delete applicationUserClone._id;

	return { ...domainUser, ...applicationUserClone };
};

const convertToDomain = (userModel) => {
	return util.isArray(userModel)
		?
		userModel.map(u => generateUser(u._doc, new User(u.Username, u.Password, u.id, u.Applications)))
		:
		generateUser(userModel._doc, new User(userModel.Username, userModel.Password, userModel.id, userModel.Applications));
};


const userSelector = user => {
	return {
		Username: user.Username
	};
};

const saveEntities = save(UserModel, convertToModels(convertToModel), userSelector);

const fetchUser = async (condition = new Object()) => {
	const userdoc = await fetch(UserModel)(condition);
	return userdoc ? convertToDomain(userdoc) : null;
};

const removeUser = async (condition = new Object()) => await remove(UserModel)(condition);

const saveUser = async entities => {
	const savedEntity = await saveEntities(entities);
	return savedEntity ? convertToDomain(savedEntity) : undefined;
};

module.exports = {
	UserModel: UserModel,
	convertToDomain: convertToDomain,
	convertToModel: convertToModels(convertToModel),
	generateUser: generateUser,
	save: saveUser,
	fetch: fetchUser,
	remove: removeUser,
};