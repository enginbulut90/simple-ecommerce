const restify = require('restify');
const router = new (require('restify-router')).Router();
const server = restify.createServer({
	name: 'iMoba Application Auth API',
	version: '1.0.0',
});

const logger = require('./basic-logger');

const account = require('./routes/account');
const user = require('./routes/user');
const application = require('./routes/application');

server.use(restify.plugins.throttle({
	burst: 100,  	// Max 10 concurrent requests (if tokens)
	rate: 12,  		// Steady state: 2 request / 1 seconds
	ip: true,		// throttle per IP
}));

server.use(restify.plugins.bodyParser());
server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.queryParser());
server.use(restify.plugins.gzipResponse());

router.add('/api/applications', application);
router.add('/api/accounts', account);
router.add('/api/users', user);
router.applyRoutes(server);

server.on('after', restify.plugins.metrics({ server: server }, function onMetrics(err, metrics) {
	logger.trace(`${metrics.method} ${metrics.path} ${metrics.statusCode} ${metrics.latency} ms`);
	console.log(`${metrics.method} ${metrics.path} ${metrics.statusCode} ${metrics.latency} ms`);
}));

server.listen(1453 , function () {
	logger.info('%s listening at %s', server.name, server.url);
	console.log('%s listening at %s', server.name, server.url);
});

server.on('uncaughtException', function (req, res, route, err) {
	logger.error(err);
	console.log(err);
});